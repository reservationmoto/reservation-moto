<?php include "header.php" ?>
<?php require 'Reservation.php';

if(is_null($_POST['name']) == false && is_null($_POST['surname']) == false && is_null($_POST['mail']) == false && is_null($_POST['phone']) == false && is_null($_POST['date']) == false) {
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $mail = $_POST['mail'];
    $phone = $_POST['phone'];
    $date = $_POST['date'];
    $reservation = new Reservation();
    $reservation->setName($name);
    $reservation->setSurname($surname);
    $reservation->setMail($mail);
    $reservation->setPhone($phone);
    $reservation->setDate($date);

    if(Reservation::getDisponnibility() != null){
        foreach(Reservation::getDisponnibility() as $disponnibility) {
            if($date == $disponnibility) { ?>
                <div class="alert alert-danger">
                    <strong>Cette date est déjà sélectionnée.</strong>
                </div>
            <?php } else {
                Reservation::setReservation($reservation);?>
                <div class="alert alert-success">
                    <strong>Votre réservation à été enregistrée !</strong>
                </div> <?php
            }
        }
    } else {
        Reservation::setReservation($reservation);?>
        <div class="alert alert-success">
            <strong>Votre réservation à été enregistrée !</strong>
        </div> <?php
    }
} else {?>
    <div class="alert alert-danger">
        <strong>Veuillez remplir tous les champs demandés !</strong>
    </div><?php
}?>

<section class="acc_slide">
    <h1>Réservez votre moto dès maintenant !</h1>
</section>

<section class="container acc_form">
    <h2 class="text-center">Intéressé ? Remplissez le formulaire</h2>
    <form method="POST" action="">
        <div class="row">
            <div class="col-sm-6">
                <input type="text" class="form-control" name="name" placeholder="Nom">
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="surname" placeholder="Prénom">
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-sm-6">
                <input type="email" class="form-control" name="mail" placeholder="Mail">
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="phone" placeholder="Téléphone">
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-sm-12">
                <input type="date" class="form-control" name="date" placeholder="Date réservation">
            </div>
        </div>

        <br><br>

        <div class="text-center">
            <button type="submit" class="btn btn-dark btn-lg">Je réserve !</button>
        </div>

    </form>
</section>

<?php include "footer.php" ?>
