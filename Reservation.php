<?php

class Reservation {
    private $id;
    private $name;
    private $surname;
    private $mail;
    private $phone;
    private $date;   

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of surname
     */ 
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set the value of surname
     *
     * @return  self
     */ 
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get the value of mail
     */ 
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set the value of mail
     *
     * @return  self
     */ 
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get the value of phone
     */ 
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @return  self
     */ 
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public static function setReservation($reservation) {
        try {
            $bdd = new PDO('mysql:host=localhost;port=8888;dbname=reservation-moto;charset=utf8', 'root', 'root');
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }

        $request = $bdd->prepare('INSERT INTO reservation(name, surname, mail, phone, date) VALUES (:name, :surname, :mail, :phone, :date)');
        $request->execute(array(
            'name' => $reservation->getName(),
            'surname' => $reservation->getSurname(),
            'mail' => $reservation->getMail(),
            'phone' => $reservation->getPhone(),
            'date' => $reservation->getDate()
        ));
    }

    public static function getDisponnibility() {
        try {
            $bdd = new PDO('mysql:host=localhost;port=8888;dbname=reservation-moto;charset=utf8', 'root', 'root');
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }

        $result = $bdd->query('SELECT * FROM reservation');
        $disponnibilities = array();
        while($datas = $result->fetch()) {
            $reservation = $datas['date'];
            array_push($disponnibilities, $reservation);
        }
        return $disponnibilities;
    }
}